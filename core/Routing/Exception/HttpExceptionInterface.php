<?php

namespace Routing\Exception;

/**
 * Interface HttpExceptionInterface
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
interface HttpExceptionInterface
{
    /**
     * @return int
     */
    public function getStatusCode();

    /**
     * @return array
     */
    public function getHeaders();
}
