<?php

namespace Routing\Exception;

/**
 * Class HttpException
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
class HttpException extends \RuntimeException implements HttpExceptionInterface
{
    /**
     * @var int 
     */
    protected $statusCode;

    /**
     * @var array 
     */
    protected $headers;

    /**
     * HttpException constructor.
     *
     * @param int             $statusCode
     * @param string|null     $message
     * @param \Exception|null $previous
     * @param array           $headers
     * @param int|null        $code
     */
    public function __construct(int $statusCode, string $message = null, \Exception $previous = null, array $headers = [], int $code = 0)
    {
        $this->statusCode = $statusCode;
        $this->headers    = $headers;
        
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }
}
