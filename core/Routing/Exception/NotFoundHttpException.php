<?php

namespace Routing\Exception;

/**
 * Class NotFoundHttpException
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
class NotFoundHttpException extends HttpException
{
    /**
     * NotFoundHttpException constructor.
     *
     * @param string     $message
     * @param \Exception $previous
     * @param array      $headers
     * @param int        $code
     */
    public function __construct(string $message = null, \Exception $previous = null, array $headers = [], int $code = 0)
    {
        parent::__construct(404, $message, $previous, $headers, $code);
    }
}
