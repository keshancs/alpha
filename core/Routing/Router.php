<?php

namespace Routing;

use App\AppKernel;
use DependencyInjection\ContainerAware;
use Http\Request;
use Http\Response;
use Kernel\AbstractKernel;
use Routing\Exception\NotFoundHttpException;
use Storage\Bucket;

/**
 * Class Router
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
class Router extends ContainerAware
{
    /**
     * @var array
     */
    private $routes = [];

    /**
     * @var array
     */
    private $route;

    /**
     * Router constructor.
     */
    public function __construct()
    {
        $this->routes = AppKernel::requirePath('@config/routes.php');
    }

    /**
     * @param Request $request
     *
     * @return Router
     */
    public function matchRequest(Request $request)
    {
        $regex         = '([^\/]+)';
        $pattern       = sprintf('#\:%s#', $regex);
        $pathInfo      = $request->getPathInfo();

        foreach ($this->routes as $name => $route) {
            $matches = [];
            $match   = sprintf('#^%s$#', preg_replace($pattern, $regex, $route['slug']));
            $matched = preg_match_all($match, $pathInfo['path'], $matches);

            array_shift($matches);

            if ($matched && $request->getMethod() == ($route['method'] ?? 'GET')) {
                $arguments          = $matches ? array_values(call_user_func_array('array_merge', $matches)) : $matches;
                $route['arguments'] = $arguments;

                $this->route = $route;

                return $this;
            }
        }

        throw new NotFoundHttpException('Route not found');
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    public function dispatch(Request $request)
    {
        list ($controller, $action) = explode('@', $this->route['uses'] ?? '@');

        $uri = $request->getUri();

        if ($uri !== '/') {
            $segments   = explode('/', trim($uri, '/'));

            $controller = $controller ?: $segments[0] ?? null;
            $action     = $action     ?: $segments[1] ?? null;
        }

        $controller = ucfirst($controller ?: AppKernel::$config['routing.default_controller']);
        $action     = $action             ?: AppKernel::$config['routing.default_action'];

        if (!$controller) {
            throw new \Exception('Unable to figure out a controller');
        }

        $controllerNamespace = sprintf('App\\Controller\\%sController', $controller);
        $actionName          = sprintf('%sAction', $action ?: 'index');

        $controllerInstance = new $controllerNamespace($this->container);

        $response = call_user_func_array([$controllerInstance, $actionName], $this->route['arguments']);
        if (!$response instanceof Response) {
            throw new \Exception(sprintf('%s::%s must return a Response', $controllerNamespace, $actionName));
        }

        die($response->send());
    }

    /**
     * @return array
     */
    public function getRoute()
    {
        return $this->route;
    }
}
