<?php

namespace Storage;

use ArrayIterator;

/**
 * Class Bucket
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
class Bucket implements \IteratorAggregate, \Countable
{
    /**
     * @var array 
     */
    protected $parameters = [];

    /**
     * Bucket constructor.
     *
     * @param array $parameters
     */
    public function __construct(array $parameters = [])
    {
        $this->parameters = $parameters;
    }

    /**
     * @param array $parameters
     */
    public function setParameters(array $parameters = [])
    {
        $this->parameters = $parameters;
    }

    /**
     * @param string $parameter
     *
     * @return mixed|null
     */
    public function get(string $parameter)
    {
        $value    = null;
        $segments = explode('.', $parameter);

        while (null !== ($segment = array_shift($segments))) {
            if (!is_array($value)) {
                $value = $this->parameters[$segment] ?? null;
                continue;
            }

            $value = $value[$segment] ?? null;
        }

        return $value;
    }

    /**
     * @param string $parameter
     * @param mixed  $value
     */
    public function set(string $parameter, $value)
    {
        $this->parameters[$parameter] = $value;
    }

    /**
     * @return array
     */
    public function all()
    {
        return $this->parameters;
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        return new ArrayIterator($this->parameters);
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return count($this->parameters);
    }
}