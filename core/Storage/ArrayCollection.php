<?php

namespace Storage;

/**
 * Class ArrayCollection
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
class ArrayCollection implements \Countable, \ArrayAccess, \Iterator
{
    /**
     * @var array 
     */
    protected $elements = [];

    /**
     * @var array 
     */
    protected $offsets = [];

    /**
     * @var int 
     */
    protected $count = 0;

    /**
     * @var int 
     */
    protected $current = 0;

    /**
     * @param $element
     *
     * @return ArrayCollection
     */
    public function add($element)
    {
        return $this->offsetSet($this->count, $element);
    }

    /**
     * @param $element
     *
     * @return ArrayCollection
     */
    public function remove($element)
    {
        $hash = $this->createHash($element);
        
        if ($this->contains($element, $hash)) {
            $index = array_search($hash, $this->offsets);
            
            return $this->offsetUnset($index, $hash);
        }
        
        return $this;
    }

    /**
     * @param mixed $element
     *
     * @param null  $hash
     *
     * @return bool
     */
    public function contains($element, $hash = null)
    {
        $hash = $hash ?: $this->createHash($element);
        
        return isset($this->elements[$hash]);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->elements;
    }

    /**
     * {@inheritdoc}
     */
    public function current()
    {
        return $this->elements[$this->key()];
    }

    /**
     * {@inheritdoc}
     */
    public function next()
    {
        $this->current++;
    }

    /**
     * {@inheritdoc}
     */
    public function key()
    {
        return $this->offsets[$this->current];
    }

    /**
     * {@inheritdoc}
     */
    public function valid()
    {
        return $this->current >= 0 && $this->current < $this->count;
    }

    /**
     * {@inheritdoc}
     */
    public function rewind()
    {
        $this->current = 0;
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        return isset($this->offsets[$offset]);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        $hash = $this->offsets[$offset];
        
        return $this->elements[$hash];
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        $hash = $this->createHash($value);
    
        $this->offsets[$offset] = $hash;
        $this->elements[$hash]  = $value;
        
        $this->count++;
        
        return $this;
    }

    /**
     * @param mixed $offset
     * @param null  $hash
     */
    public function offsetUnset($offset, $hash = null)
    {
        $hash = $hash ?: $this->offsets[$offset];
        
        unset($this->elements[$hash], $this->offsets[$offset]);
        
        $this->offsets = array_values($this->offsets);
        
        $this->count--;
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return $this->count;
    }

    /**
     * @param mixed $element
     *
     * @return string
     */
    protected function createHash($element)
    {
        return md5(is_object($element) ? spl_object_id($element) : $element);
    }
}
