<?php

namespace Database;

use App\AppKernel;
use Kernel\AbstractKernel;

/**
 * Class Database
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
class Connection
{
    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * Database constructor.
     */
    public function __construct()
    {
        $options = [
            \PDO::MYSQL_ATTR_INIT_COMMAND => sprintf('SET NAMES %s', AppKernel::$config['db.charset']),
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_OBJ
        ];

        return $this->pdo = new \PDO($this->getDsn(), AppKernel::$config['db.username'], AppKernel::$config['db.password'], $options);
    }

    /**
     * @return \PDO
     */
    public function getPdo()
    {
        return $this->pdo;
    }

    /**
     * @return string
     */
    private function getDsn()
    {
        return sprintf('%s:host=%s;dbname=%s', AppKernel::$config['db.driver'], AppKernel::$config['db.hostname'], AppKernel::$config['db.name']);
    }
}
