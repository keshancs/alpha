<?php

namespace View;

use Http\Response;

/**
 * Class View
 */
class View implements ViewInterface
{
    /**
     * {@inheritdoc}
     */
    public function render(string $path, array $parameters = [])
    {
        return new Response(__METHOD__);
    }
}
