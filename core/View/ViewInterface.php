<?php

namespace View;

/**
 * Interface ViewInterface
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
interface ViewInterface
{
    /**
     * Renders a view and returns a response.
     *
     * @param string $path
     * @param array  $parameters
     *
     * @return mixed
     */
    public function render(string $path, array $parameters = []);
}
