<?php

namespace Http;

/**
 * Class JsonResponse
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
class JsonResponse extends Response
{
    /**
     * JsonResponse constructor.
     *
     * @param array $body
     * @param int   $code
     */
    public function __construct(array $body = [], int $code = 200)
    {
        header('Content-Type: application/json');
        
        parent::__construct(json_encode($body), $code);
    }
}