<?php

namespace Http;

use Storage\Bucket;

/**
 * Class Request
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
class Request
{
    /**
     * @var Bucket
     */
    public $get;

    /**
     * @var Bucket
     */
    public $post;
    
    /**
     * @var Bucket
     */
    public $server;

    /**
     * @var Bucket
     */
    public $query;

    /**
     * @var array 
     */
    private $pathInfo;

    /**
     * Request constructor.
     *
     * @param array $get
     * @param array $post
     * @param array $server
     */
    public function __construct(array $get, array $post, array $server)
    {
        $this->get    = new Bucket($get);
        $this->post   = new Bucket($post);
        $this->server = new Bucket($server);
        
        $this->pathInfo = $this->preparePathInfo();
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->server->get('REQUEST_URI');
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->server->get('REQUEST_METHOD');
    }

    /**
     * @return array
     */
    public function getPathInfo()
    {
        return $this->pathInfo;
    }

    /**
     * @return array
     */
    private function preparePathInfo()
    {
        $uri         = $this->server->get('REQUEST_URI');
        $parseUrl    = parse_url($uri);
        $query       = [];

        parse_str($parseUrl['query'] ?? '', $query);

        $this->query = new Bucket($query);

        return $parseUrl;
    }

    /**
     * @return Request
     */
    public static function createFromGlobals()
    {
        return new self($_GET, $_POST, $_SERVER);
    }
}
