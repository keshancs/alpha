<?php

namespace Http;

/**
 * Class Response
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
class Response
{
    /**
     * @var string
     */
    protected $body;

    /**
     * @var int 
     */
    protected $code;

    /**
     * Response constructor.
     *
     * @param string $body
     * @param int    $code
     */
    public function __construct(string $body = '', int $code = 200)
    {
        $this->body = $body;
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     *
     * @return Response
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @return string
     */
    public function send()
    {
        http_response_code($this->code);

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getBody();
    }
}
