<?php

namespace Kernel;

/**
 * Interface KernelInterface
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
interface KernelInterface
{
    /**
     * Creates an instance.
     *
     * @param string $root
     *
     * @return KernelInterface
     */
    public static function create(string $root);

    /**
     * Registers exception handler.
     *
     * @return KernelInterface
     */
    public function registerExceptionHandler();

    /**
     * Defines path aliases.
     *
     * @param string $rootDir
     *
     * @return KernelInterface
     */
    public function defineAliases(string $rootDir);

    /**
     * Initializes.
     */
    public function init();

    /**
     * Dispatches.
     */
    public function dispatch();
}
