<?php

namespace Kernel;

use DependencyInjection\Container;
use DependencyInjection\ContainerAware;
use Http\Response;
use Routing\Exception\HttpExceptionInterface;
use Service\PathResolver;
use Service\ServiceLoader;

/**
 * Class Kernel
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
abstract class AbstractKernel extends ContainerAware implements KernelInterface
{
    /**
     * @var array
     */
    public static $config = [];

    /**
     * @var array
     */
    protected static $aliases = [];

    /**
     * @var ServiceLoader
     */
    protected $serviceLoader;

    /**
     * @var string
     */
    protected $exceptionHandler = 'handleException';

    /**
     * Kernel constructor.
     *
     * @param string $rootDir
     */
    public function __construct(string $rootDir)
    {
        $this->registerExceptionHandler()->defineAliases($rootDir)->loadConfig();
        $this->setContainer($this->container ?: Container::getInstance());

        $this->serviceLoader = new ServiceLoader($this->container);
    }

    /**
     * {@inheritdoc}
     */
    public static function create(string $root)
    {
        $class  = get_called_class();
        /** @var KernelInterface $kernel */
        $kernel = new $class($root);

        return $kernel->init();
    }

    /**
     * @param string $alias
     *
     * @return string
     */
    public static function getAlias(string $alias)
    {
        return static::$aliases[$alias] ?? null;
    }

    /**
     * @param string $alias
     * @param string $path
     */
    public static function setAlias(string $alias, string $path)
    {
        static::$aliases[$alias] = $path;
    }

    /**
     * @param string $path
     *
     * @return mixed
     */
    public static function requirePath(string $path)
    {
        return require self::getPath($path);
    }

    /**
     * @param string $path
     *
     * @return string|null
     */
    public static function getPath(string $path)
    {
        $pathResolver = PathResolver::create(self::class);

        if (!$pathResolver->isResolvedPath($path)) {
            return $pathResolver->resolvePath($path);
        }

        return $pathResolver->getResolvedPath($path);
    }

    /**
     * @return KernelInterface
     */
    public function init()
    {
        $services = self::requirePath('@app/config/services.php');

        $this->serviceLoader->loadAll($services);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function registerExceptionHandler()
    {
        set_exception_handler([$this, $this->exceptionHandler]);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function defineAliases(string $rootDir)
    {
        self::setAlias('@root', realpath($rootDir));
        self::setAlias('@app', '@root/app');
        self::setAlias('@config', '@app/config');

        return $this;
    }

    /**
     * @return KernelInterface
     */
    public function loadConfig()
    {
        static::$config = static::requirePath('@config/config.php');

        return $this;
    }

    /**
     * @param \Throwable $e
     */
    public function handleException(\Throwable $e)
    {
        ob_end_flush();

        $parameters = [
            'e'     => $e,
            'class' => get_class($e),
        ];

        $html       = $this->container->get('view')->render('exception.html.twig', $parameters);
        $statusCode = $e instanceof HttpExceptionInterface ? $e->getStatusCode() : 500;
        $response   = new Response($html, $statusCode);

        die($response->send());
    }

    /**
     *
     */
    public function dispatch()
    {
        $request = $this->container->get('request');

        $this->container->get('router')->matchRequest($request)->dispatch($request);
    }
}
