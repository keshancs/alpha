<?php

namespace Controller;

use App\AppKernel;
use DependencyInjection\ContainerInterface;
use Http\Response;
use Kernel\AbstractKernel;

/**
 * Class Controller
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
class Controller
{
    /**
     * @var string
     */
    protected $id;
    
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Controller constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        
        $reflection = new \ReflectionClass(get_called_class());
        $this->id   = strtolower(str_replace('Controller', '', $reflection->getShortName()));
    }

    /**
     * @param string $view
     * @param array  $parameters
     *
     * @return Response
     */
    public function render(string $view, array $parameters = [])
    {
        $path   = implode('/', [$this->id, $view]);
        $engine = AppKernel::$config['templating.engine'] ?? 'view';
        $html   = $this->container->get($engine)->render($path, $parameters);
        
        return new Response($html);
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    protected function get(string $name)
    {
        return $this->container->get($name);
    }
}
