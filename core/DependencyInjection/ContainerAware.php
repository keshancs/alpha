<?php

namespace DependencyInjection;

/**
 * Class ContainerAware
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
class ContainerAware implements ContainerAwareInterface
{
    use ContainerAwareTrait;
}
