<?php

namespace DependencyInjection;

/**
 * Class ContainerAwareInterface
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
interface ContainerAwareInterface
{
    /**
     * @param ContainerInterface $container
     *
     * @return ContainerAwareInterface
     */
    public function setContainer(ContainerInterface $container);
}
