<?php

namespace DependencyInjection;

/**
 * Class Container
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
class Container implements ContainerInterface
{
    /**
     * @var Container|null
     */
    private static $instance;

    /**
     * @var array 
     */
    private $services = [];

    /**
     * {@inheritdoc}
     */
    public static function getInstance()
    {
        if (null == static::$instance) {
            static::$instance = new self();
            static::$instance->register();
        }
        
        return static::$instance;
    }

    /**
     * Container constructor.
     */
    public function __construct()
    {
        if (!is_null(static::$instance)) {
            throw new \Exception('Container cannot be instantiated more than once');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function register()
    {
        $this->services['container'] = $this;
        
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function set(string $name, $service)
    {
        $this->services[$name] = $service;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $name)
    {
        return $this->services[$name] ?? null;
    }
}
