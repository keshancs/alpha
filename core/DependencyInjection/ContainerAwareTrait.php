<?php

namespace DependencyInjection;

/**
 * Trait ContainerAwareTrait
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
trait ContainerAwareTrait
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * {@inheritdoc}
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;

        return $this;
    }
}