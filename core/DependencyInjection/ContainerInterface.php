<?php

namespace DependencyInjection;

/**
 * Class ContainerInterface
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
interface ContainerInterface
{
    /**
     * @return ContainerInterface
     */
    public static function getInstance();
    
    /**
     * @return ContainerInterface
     */
    public function register();

    /**
     * @param string $name
     * @param        $service
     *
     * @return mixed
     */
    public function set(string $name, $service);

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function get(string $name);
}
