<?php

namespace Service;

use Http\Kernel;
use Kernel\KernelInterface;

/**
 * Class PathResolver
 */
class PathResolver
{
    /**
     * @var array
     */
    private static $instances = [];

    /**
     * @var string
     */
    private $class;

    /**
     * @var array
     */
    private $resolvedPaths = [];

    /**
     * @param string $class
     *
     * @return mixed
     */
    public static function create(string $class)
    {
        if (!isset(static::$instances[$class])) {
            static::$instances[$class] = new self($class);
        }

        return static::$instances[$class];
    }

    /**
     * PathResolver constructor.
     *
     * @param string $class
     */
    public function __construct(string $class)
    {
        if (!in_array(KernelInterface::class, class_implements($class))) {
            throw new \Exception(sprintf('%s class must implement a %s interface', $class, KernelInterface::class));
        }

        $this->class = $class;
    }

    /**
     * @param string $path
     *
     * @return string
     */
    public function resolvePath(string $path)
    {
        if (!$resolvedPath = $this->class::getAlias($path)) {
            $resolvedPath = $path;
        }

        while (0 === strpos($resolvedPath, '@')) {
            $segments = explode('/', $resolvedPath);

            if ($segment = $this->class::getAlias($segments[0])) {
                $segments[0] = $segment;
            }

            $resolvedPath = implode('/', $segments);
        }

        if (!file_exists($resolvedPath)) {
            return $resolvedPath;
        }

        return $this->resolvedPaths[$path] = $resolvedPath;
    }

    /**
     * @param string $path
     *
     * @return bool
     */
    public function isResolvedPath(string $path)
    {
        return isset($this->resolvedPaths[$path]);
    }

    /**
     * @param string $path
     *
     * @return string
     */
    public function getResolvedPath(string $path)
    {
        return $this->resolvedPaths[$path];
    }
}
