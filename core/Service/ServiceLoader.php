<?php

namespace Service;

use DependencyInjection\ContainerAwareInterface;
use DependencyInjection\ContainerInterface;

/**
 * Class ServiceLoader
 */
class ServiceLoader
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var array
     */
    private $queue = [];

    /**
     * ServiceLoader constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param array $services
     *
     * @return ServiceLoader
     */
    public function loadAll(array $services)
    {
        foreach ($services as $name => $service) {
            $this->load($name, $service);
        }

        return $this;
    }

    /**
     * @param string $name
     * @param array  $service
     *
     * @return ServiceLoader
     */
    public function load(string $name, array $service)
    {
        $arguments = [];

        if (isset($service['arguments'])) {
            foreach ($service['arguments'] as $argument) {
                if ($this->isServiceArgument($argument)) {
                    $argumentServiceName = $this->getArgumentServiceName($argument);
                    $requiredService     = $this->container->get($argumentServiceName);

                    if (!$requiredService) {
                        return $this->addToLoadQueue($argumentServiceName, $name, $service);
                    }

                    $arguments[] = $requiredService;
                }
            }
        }

        if (isset($service['constructor'])) {
            $instance = $service['class']::{$service['constructor']}();
        } else {
            $reflectionClass = new \ReflectionClass($service['class']);
            $instance        = $reflectionClass->newInstanceArgs($arguments);
        }

        if (in_array(ContainerAwareInterface::class, class_implements($instance))) {
            $instance->setContainer($this->container);
        }

        $this->container->set($name, $instance);

        $this->loadQueue($name);

        return $this;
    }

    /**
     * @param string $serviceName
     *
     * @return $this
     */
    private function loadQueue(string $serviceName)
    {
        if (isset($this->queue[$serviceName])) {
            foreach ($this->queue[$serviceName] as $name => $service) {
                $this->load($name, $service);
            }
        }

        return $this;
    }

    /**
     * @param string $serviceName
     * @param string $name
     * @param array  $service
     *
     * @return $this
     */
    private function addToLoadQueue(string $serviceName, string $name, array $service)
    {
        $this->queue[$serviceName][$name] = $service;

        return $this;
    }

    /**
     * @param string $argument
     *
     * @return false|int
     */
    private function isServiceArgument(string $argument)
    {
        return preg_match('#^\@#', $argument);
    }

    /**
     * @param string $argument
     *
     * @return string|string[]|null
     */
    private function getArgumentServiceName(string $argument)
    {
        return preg_replace('#^\@(.*)$#', '$1', $argument);
    }
}
