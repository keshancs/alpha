<?php

require __DIR__ . '/../vendor/autoload.php';

\App\AppKernel::create(__DIR__ . '/..')->dispatch();
