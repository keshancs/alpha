<?php

namespace App;

use Kernel\AbstractKernel;

/**
 * Class AppKernel
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
class AppKernel extends AbstractKernel
{
}
