<?php

namespace App\Controller;

use Controller\Controller;
use Http\Response;

/**
 * Class DefaultController
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
class DefaultController extends Controller
{
    /**
     * @return Response
     */
    public function indexAction()
    {
        return new Response('Hello World!');
    }
}
