<?php

namespace App\Controller;

use Controller\Controller;
use Http\JsonResponse;

/**
 * Class HelloController
 *
 * @author Georgijs Kļaviņš <georgijs.klavins@gmail.com>
 */
class HelloController extends Controller
{
    /**
     * @param string $name
     *
     * @return JsonResponse
     */
    public function worldAction(string $name)
    {
        return new JsonResponse(['name' => $name]);
    }
}
