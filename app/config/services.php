<?php

return [
    'logger' => [
        'class' => \Logger\Logger::class,
    ],
    'request' => [
        'class' => \Http\Request::class,
        'constructor' => 'createFromGlobals',
    ],
    'db' => [
        'class' => \Database\Connection::class,
        'arguments' => ['@view', '@router'],
    ],
    'router' => [
        'class' => \Routing\Router::class,
    ],
    'view' => [
        'class' => \View\View::class,
    ]
];
